def calculateSum(value):
    summ = 0
    val = value
    while val // 10 > 0:
        val //= 10
        summ += val % 10
    return summ


def calculateCount(value):
    count = 0
    val = value
    while val / 10 > 0:
        val //= 10
        count += 1
    return count


print(f'Сумма чисел {calculateSum(500)}')
print(f'Количество цифр в числе {calculateCount(500)}')
print(f'Разность суммы и количества цифр {calculateSum(500) - calculateCount(500)}')
