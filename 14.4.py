def number_back(n):
    parts = str(n).split('.')
    list_1 = parts[0]
    list_2 = parts[1]
    reversedValue_1 = ''
    reversedValue_2 = ''
    for i in range(len(str(parts[0])), 0, -1):
        reversedValue_1 += parts[0][i - 1]
    for j in range(len(str(parts[1])), 0, -1):
        reversedValue_2 += parts[1][j - 1]
    return reversedValue_1 + '.' + reversedValue_2

first = float(input('Введите первое число: '))
second = float(input('Введите второе число: '))

first_back = number_back(first)
second_back = number_back(second)

print('Первое число наоборот:', first_back)
print('Второе число наоборот:', second_back)
print('Сумма:', float(first_back) + float(second_back))
